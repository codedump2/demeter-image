Demeter Docker Image
====================

This creates a Fedora based docker image of the
[https://bruceravel.github.io/demeter/](Demeter) X-Ray data analysis
tool.
