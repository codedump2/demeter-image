FROM registry.fedoraproject.org/fedora-toolbox:39

COPY patches/ifeffit-pgplot-png12.patch /tmp/ifeffit-pgplot-png12.patch

RUN dnf install --assumeyes \
	git \
	patch diffutils \
	perl \
	curl wget \
	gcc-gfortran \
	libpng12-devel \
	giflib-devel \
	libX11-devel \
	ncurses-term ncurses-devel \
	perl-Module-Build \
	perl-XML-Parser \
	perl-XML-Twig \
	perl-XML-XPath \
	perl-XML-SAX-Writer \
	perl-XMLRPC-Lite \
	perl-YAML \
	perl-Cpanel-JSON-XS \
	perl-App-cpanminus \
	wxGTK \
	perl-Wx \
	perl-Wx-GLCanvas \
	perl-Number-Compare \
	perl-Net-Server \
	perl-File-Touch \
	mesa-libGL-devel \
	mesa-libGLU-devel \
	mesa-libGLw-devel \
	mesa-libOpenCL-devel \
	mesa-libOSMesa-devel \
	freeglut-devel \
	libXi-devel \
	bzip2 \
	hdf hdf-devel hdf-libs hdf-static \
	hdf5 hdf5-devel hdf5-static \
        libGL libGLU gtk3

RUN	mkdir /tmp/dem && cd /tmp/dem && \
	git clone https://github.com/newville/ifeffit.git && \
	cd ifeffit && \
	wget -O pgplot5.2.tar.gz ftp://ftp.astro.caltech.edu/pub/pgplot/pgplot5.2.tar.gz && \
	patch PGPLOT_install < /tmp/ifeffit-pgplot-png12.patch && \
	./PGPLOT_install && \
	\
	./configure && \
	make && \
	make install && \
	\
	cpanm -vn Alien::HDF4 && \
	cpanm -vn PDL::Stats::GLM && \
	cpanm -vn File::Monitor::Lite
	

RUN	cd /tmp/dem && \
	git clone https://github.com/bruceravel/demeter.git && \
	cd demeter && \
	perl -I. ./Build.PL && \
	(PERL_MM_USE_DEFAULT=1 echo n | ./Build installdeps) && \
	CFLAGS=-Wno-format-security ./Build && \
	./Build install && \
	\
	cd / && \
	rm -rf /tmp/dem && \
	\
	dnf --assumeyes install gnuplot gnuplot-wx && \
	cpanm -vn Graphics::GnuplotIF

# for xrayutilities
RUN dnf install --assumeyes \
	python3-scipy \
	python3-numpy \
	python3-h5py \
	pip \
	&& \
	pip install xrayutilities

##
## Vesta from JPMinterals (http://www.jp-minerals.org/vesta/en/download.html)
##

RUN wget http://www.jp-minerals.org/vesta/archives/3.5.8/vesta-3.5.8-1.x86_64.rpm -O /tmp/vesta-3.5.8-1.x86_64.rpm && \
    rpm -i /tmp/vesta-3.5.8-1.x86_64.rpm && \
    rm -rf /tmp/vesta-3.5.8-1.x86_64.rpm

LABEL   com.github.containers.toolbox="true"
ENV VERSION=39
